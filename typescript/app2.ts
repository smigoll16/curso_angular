import Pessoa from './Pessoa';
import Carro from './Carro';
import Concessionaria from './Concessionaria';


/* --- criar carros ---*/

let carroA = new Carro('Uno', 2);
let carroB = new Carro('Veloster', 3);
let carroC = new Carro('Civic', 4);


/* --- criar carros ---*/

let listaDeCarros: Carro[] = [carroA, carroB, carroC];

let concessionaria = new Concessionaria('Av Paulista', listaDeCarros);

/* --- exibir a lista de carros ---*/

//console.log(concessionaria.mostrarListaDeCarros());

/* --- comprar o carro ---*/

let cliente = new Pessoa('Marcos', 'Civic');

concessionaria.mostrarListaDeCarros().map((carro: Carro) => {
    
    if(carro['modelo'] == cliente.dizerCarroPreferido()) {
        
        cliente.comprarCarro(carro);

    }

});

console.log(cliente.dizerCarroQueTem());