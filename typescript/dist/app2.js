"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Carro_1 = require("./Carro");
/* --- criar carros ---*/
var carroA = new Carro_1.Carro('Uno', 2);
var carroB = new Carro_1.Carro('Veloster', 3);
var carroC = new Carro_1.Carro('Civic', 4);
/* --- criar carros ---*/
var listaDeCarros = [carroA, carroB, carroC];
var concessionaria = new Concessionaria('Av Paulista', listaDeCarros);
/* --- exibir a lista de carros ---*/
//console.log(concessionaria.mostrarListaDeCarros());
/* --- comprar o carro ---*/
var cliente = new Pessoa('Marcos', 'Civic');
concessionaria.mostrarListaDeCarros().map(function (carro) {
    if (carro['modelo'] == cliente.dizerCarroPreferido()) {
        cliente.comprarCarro(carro);
    }
});
console.log(cliente.dizerCarroQueTem());
